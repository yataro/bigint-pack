import {describe, it} from 'node:test';
import assert from 'node:assert';
import {BigIntPack, BigIntPackAlloc, BigIntTooBigError, BigIntUnpack, BoundsError, MAX_PACKED_LENGTH} from '../lib/index.js';

function assertHexEqual(buffer, hex, offset = 0, length = undefined) {
	assert.equal(buffer.subarray(offset, length).toString('hex'), hex);
}

describe('BigInt packing/unpacking', () => {
	it('should pack', () => {
		let packed = BigIntPackAlloc(0n);
		assertHexEqual(packed, '00');

		packed = BigIntPackAlloc(1024n);
		assertHexEqual(packed, '020004');

		packed = BigIntPackAlloc(1267650600228229401496703205376n);
		assertHexEqual(packed, '0d00000000000000000000000010');
	});

	it('should pack negative values', () => {
		let packed = BigIntPackAlloc(-1024n);
		assertHexEqual(packed, '820004');

		packed = BigIntPackAlloc(-1267650600228229401496703205376n);
		assertHexEqual(packed, '8d00000000000000000000000010');
	});

	it('should unpack', () => {
		let buf = Buffer.from('00', 'hex');
		let bint = BigIntUnpack(buf);
		assert.strictEqual(bint, 0n);

		buf = Buffer.from('020004', 'hex');
		bint = BigIntUnpack(buf);
		assert.strictEqual(bint, 1024n);

		buf = Buffer.from('0d00000000000000000000000010', 'hex');
		bint = BigIntUnpack(buf);
		assert.strictEqual(bint, 1267650600228229401496703205376n)
	});

	it('should unpack negative values', () => {
		let buf = Buffer.from('820004', 'hex');
		let bint = BigIntUnpack(buf);
		assert.strictEqual(bint, -1024n);

		buf = Buffer.from('8d00000000000000000000000010', 'hex');
		bint = BigIntUnpack(buf);
		assert.strictEqual(bint, -1267650600228229401496703205376n)
	});

	it('should fail to pack', () => {
		let buf = Buffer.alloc(0);

		assert.throws(() => {
			BigIntPack(0n, buf);
		}, BoundsError);

		buf = Buffer.alloc(1);

		assert.throws(() => {
			BigIntPack(1024n, buf);
		}, BoundsError);

		buf = Buffer.alloc(MAX_PACKED_LENGTH);

		assert.throws(() => {
			BigIntPack(256n ** BigInt(MAX_PACKED_LENGTH), buf);
		}, BigIntTooBigError);
	});

	it('should fail to unpack', () => {
		let buf = Buffer.from('8200', 'hex');

		assert.throws(() => {
			BigIntUnpack(buf);
		}, BoundsError);
	});
});

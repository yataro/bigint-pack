import {Buffer} from 'node:buffer';
import {BigIntTooBigError, BoundsError} from './errors.js';

const BYTE_MASK =	0xFFn;
const SIGN_MASK =	0b10000000;
const LENGTH_MASK =	0b01111111;
const MAX_LENGTH =	0b01111111;
const BYTE_SHIFT =	8n;

function checkLength(len) {
	if (len > MAX_LENGTH) {
		throw new BigIntTooBigError();
	}
}

function setBoundChecked(target, offset, byte) {
	if (offset >= target.length) {
		throw new BoundsError(offset);
	}

	target[offset] = byte;
}

function getBoundChecked(target, offset) {
	const byte = target[offset];

	if (byte === undefined) {
		throw new BoundsError(offset);
	}

	return byte;
}

/**
 * An object that can be used for BigInt packing/unpacking.
 * @typedef {Buffer|Uint8Array|Uint8ClampedArray} BufferLike
 */

/**
 * Max length of packed BigInt.
 * @type {number}
 */
export const MAX_PACKED_LENGTH = 1 + MAX_LENGTH; // sign byte + max length.

/**
 * Calculate length of packed BigInt.
 * @param {BigInt} bint - BigInt to calculate length of.
 * @returns {number} Number of bytes contained within packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 */
export function BigIntByteLength(bint) {
	let len = 1;

	if (bint < 0n) {
		bint = -bint;
	}

	for (; bint !== 0n; len++) {
		bint >>= BYTE_SHIFT;
	}

	checkLength(len);

	return len;
}

/**
 * Pack BigInt to bytes.
 * @param {BigInt} bint - BigInt to pack.
 * @param {BufferLike} target - A target to pack into.
 * @param {number} [offset] - The offset within target at which to begin packing.
 * @returns {number} - Length of packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 * @throws {BoundsError} Target not satisfy offset <= target.length - packed_length.
 */
export function BigIntPack(bint, target, offset = 0) {
	let len = 0;
	let sign = 0;

	if (bint < 0) {
		sign = SIGN_MASK;

		bint = -bint;
	}

	offset++; // Skip sign byte.

	while (bint !== 0n) {
		len++;
		checkLength(len);

		setBoundChecked(target, offset, Number(bint & BYTE_MASK));
		offset++;

		bint = bint >> BYTE_SHIFT;
	}

	setBoundChecked(target, 0, len | sign);

	return 1 + len; // sign byte + length.
}

/**
 * Pack BigInt to allocated Buffer.
 * @param {BigInt} bint - BigInt to pack.
 * @returns {Buffer} Packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 */
export function BigIntPackAlloc(bint) {
	const len = BigIntByteLength(bint);

	const buffer = Buffer.allocUnsafe(len);

	BigIntPack(bint, buffer);

	return buffer;
}

/**
 * Unpack BigInt from bytes.
 * @param {BufferLike} source - Source from which to unpack.
 * @param {number} [offset] - The offset within source at which to begin unpacking.
 * @returns {BigInt} - Unpacked BigInt.
 * @throws {BoundsError} Source not satisfy offset <= source.length - packed_length.
 */
export function BigIntUnpack(source, offset = 0) {
	const sign_byte = getBoundChecked(source, offset);

	const len = sign_byte & LENGTH_MASK;
	const sign = sign_byte & SIGN_MASK;

	const end = offset + 1 + len; // offset + sign byte + length.

	if (end > source.length) {
		throw new BoundsError(source.length);
	}

	let bint = 0n;

	const start = offset + 1;

	for (offset = end - 1; offset >= start; offset--) {
		bint <<= BYTE_SHIFT;

		bint |= BigInt(source[offset]);
	}

	if (sign) {
		bint = -bint;
	}

	return bint;
}

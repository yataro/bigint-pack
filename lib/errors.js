/**
 * Error that occurs when BigInt value is too high.
 */
export class BigIntTooBigError extends Error {
	/**
	 * Create a BigIntTooBigError.
	 */
	constructor() {
		super('BigInt value too big to pack.');
	}
};

/**
 * Error that occurs when accessing invalid offset.
 */
export class BoundsError extends Error {
	/**
	 * Create a BoundsError.
	 * @param {number} offset - Accessed offset.
	 */
	constructor(offset) {
		super('Attempt to access offset outside bounds.');

		/**
		 * Accessed offset.
		 * @type {number}
		 */
		this.offset = offset;
	}
};

/**
 * Error that occurs when BigInt value is too high.
 */
export class BigIntTooBigError extends Error {
    /**
     * Create a BigIntTooBigError.
     */
    constructor();
}
/**
 * Error that occurs when accessing invalid offset.
 */
export class BoundsError extends Error {
    /**
     * Create a BoundsError.
     * @param {number} offset - Accessed offset.
     */
    constructor(offset: number);
    /**
     * Accessed offset.
     * @type {number}
     */
    offset: number;
}

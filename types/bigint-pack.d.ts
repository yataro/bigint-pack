/**
 * Calculate length of packed BigInt.
 * @param {BigInt} bint - BigInt to calculate length of.
 * @returns {number} Number of bytes contained within packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 */
export function BigIntByteLength(bint: BigInt): number;
/**
 * Pack BigInt to bytes.
 * @param {BigInt} bint - BigInt to pack.
 * @param {BufferLike} target - A target to pack into.
 * @param {number} [offset] - The offset within target at which to begin packing.
 * @returns {number} - Length of packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 * @throws {BoundsError} Target not satisfy offset <= target.length - packed_length.
 */
export function BigIntPack(bint: BigInt, target: BufferLike, offset?: number): number;
/**
 * Pack BigInt to allocated Buffer.
 * @param {BigInt} bint - BigInt to pack.
 * @returns {Buffer} Packed BigInt.
 * @throws {BigIntTooBigError} BigInt too big to pack.
 */
export function BigIntPackAlloc(bint: BigInt): Buffer;
/**
 * Unpack BigInt from bytes.
 * @param {BufferLike} source - Source from which to unpack.
 * @param {number} [offset] - The offset within source at which to begin unpacking.
 * @returns {BigInt} - Unpacked BigInt.
 * @throws {BoundsError} Source not satisfy offset <= source.length - packed_length.
 */
export function BigIntUnpack(source: BufferLike, offset?: number): BigInt;
/**
 * An object that can be used for BigInt packing/unpacking.
 * @typedef {Buffer|Uint8Array|Uint8ClampedArray} BufferLike
 */
/**
 * Max length of packed BigInt.
 * @type {number}
 */
export const MAX_PACKED_LENGTH: number;
/**
 * An object that can be used for BigInt packing/unpacking.
 */
export type BufferLike = Buffer | Uint8Array | Uint8ClampedArray;

# bigint-pack

Simple BigInt to binary packer/unpacker.

## How it works

BigInt is arbitrary integer type, and cannot be represented as a fixed-width integer.
For example BigInt `1` can be represented in memory as an object as follows:

```json
{
    "sign": 0,
    "bytes": [1]
}
```

Where `sign` is the integer sign and `bytes` is the number representation.

This packer uses `1` bit for integer sign + `7` bits for byte length, allowing up to `127` bytes to represent number.

Byte order is always little-endian.

For example, we have BigInt `-5`: Negative number that fits in `1` byte, will be represented as follows:

|Sign|Length|Bytes|
|----|------|-----|
|  1 |   1  | [5] |

And in binary form it will look like this:

```
10000001 00000101
^^       ^
||       |_byte_0_
||
||_7_bit_length_
|
|_1_bit_sign
```

## API Reference

### `BigIntByteLength(bint) → {number}`

Calculate length of packed BigInt.

#### Parameters:

| Name |  Type  |           Description          |
|------|--------|--------------------------------|
| bint | BigInt | BigInt to calculate length of. |

#### Throws:

[`BigIntTooBigError`](#biginttoobigerror) - BigInt too big to pack.

#### Returns:

Number of bytes contained within packed BigInt.

### `BigIntPack(bint, target, offset) → {number}`

Pack BigInt to bytes.

#### Parameters:

|  Name  |           Type            | Default |                     Description                     |
|--------|---------------------------|---------|-----------------------------------------------------|
|  bint  |          BigInt           |         |                    BigInt to pack.                  |
| target | [BufferLike](#bufferlike) |         |                A target to pack into.               |
| offset |          Number           |    0    | The offset within target at which to begin packing. |

#### Throws:

[`BigIntTooBigError`](#biginttoobigerror) - BigInt too big to pack.

[`BoundsError`](#boundserror) - Target not satisfy offset <= target.length - packed_length.

#### Returns:

Length of packed BigInt.

### `BigIntPackAlloc(bint) → {Buffer}`

Pack BigInt to allocated Buffer.

| Name |  Type  |   Description   |
|------|--------|-----------------|
| bint | BigInt | BigInt to pack. |

#### Throws:

[`BigIntTooBigError`](#biginttoobigerror) - BigInt too big to pack.

#### Returns:

Packed BigInt.

### `BigIntUnpack(source, offset) → {BigInt}`

Unpack BigInt from bytes.

#### Parameters:

|  Name  |            Type           | Default |                      Description                      |
|--------|---------------------------|---------|-------------------------------------------------------|
| source | [BufferLike](#bufferlike) |         |              Source from which to unpack.             |
| offset |           Number          |    0    | The offset within source at which to begin unpacking. |

#### Throws:

[`BoundsError`](#boundserror) - Source not satisfy offset <= source.length - packed_length.

#### Returns:

Unpacked BigInt.

### `BigIntTooBigError`

Error that occurs when BigInt value is too high.

### `BoundsError`

Error that occurs when accessing invalid offset.

#### Members:

`offset : number` - Accessed offset.

### `BufferLike`

Alias for Buffer or Uint8Array or Uint8ClampedArray.

### `(constant) MAX_PACKED_LENGTH : number`

Max length of packed BigInt.

## Author

[Aikawa Yataro](https://gitlab.com/yataro)
